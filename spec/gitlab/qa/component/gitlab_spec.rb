# frozen_string_literal: true

module Gitlab
  module QA
    describe Component::Gitlab do
      before do
        Runtime::Scenario.define(:omnibus_configuration, Runtime::OmnibusConfiguration.new)
        Runtime::Scenario.define(:seed_db, false)
        Runtime::Scenario.define(:seed_admin_token, true)
        Runtime::Scenario.define(:omnibus_exec_commands, [])
        Runtime::Scenario.define(:skip_server_hooks, true)
      end

      let(:full_ce_address) { 'registry.gitlab.com/foo/gitlab/gitlab-ce' }
      let(:full_ce_address_with_complex_tag) { "#{full_ce_address}:omnibus-7263a2" }

      describe '#release' do
        context 'with no release' do
          it 'defaults to CE' do
            expect(subject.release.to_s).to eq 'gitlab/gitlab-ce:nightly'
          end
        end
      end

      describe '#release=' do
        before do
          subject.release = release
        end

        context 'when release is a Release object' do
          let(:release) { create_release('CE') }

          it 'returns a correct release' do
            expect(subject.release.to_s).to eq 'gitlab/gitlab-ce:nightly'
          end
        end

        context 'when release is a string' do
          context 'with a simple tag' do
            let(:release) { full_ce_address_with_complex_tag }

            it 'returns a correct release' do
              expect(subject.release.to_s).to eq full_ce_address_with_complex_tag
            end
          end
        end
      end

      describe '#name' do
        before do
          subject.release = create_release('EE')
        end

        it 'returns a unique name' do
          expect(subject.name).to match(/\Agitlab-ee-(\w+){8}\z/)
        end
      end

      describe '#hostname' do
        it { expect(subject.hostname).to match(/\Agitlab-ce-(\w+){8}\.\z/) }

        context 'with a network' do
          before do
            subject.network = 'local'
          end

          it 'returns a valid hostname' do
            expect(subject.hostname).to match(/\Agitlab-ce-(\w+){8}\.local\z/)
          end
        end
      end

      describe '#address' do
        context 'with a network' do
          before do
            subject.network = 'local'
          end

          it 'returns a HTTP address' do
            expect(subject.address)
              .to match(%r{http://gitlab-ce-(\w+){8}\.local\z})
          end
        end
      end

      describe '#start' do
        let(:docker) { spy('docker command') }

        before do
          stub_const('Gitlab::QA::Docker::Command', docker)

          allow(subject).to receive(:ensure_configured!)
        end

        it 'runs a docker command' do
          subject.start

          expect(docker).to have_received(:execute!)
        end

        it 'dynamically binds HTTP port' do
          subject.start

          expect(docker).to have_received(:port).with("80")
        end

        it 'specifies the name' do
          subject.start

          expect(docker).to have_received(:<<)
                              .with("--name #{subject.name}")
        end

        it 'specifies the hostname' do
          subject.start

          expect(docker).to have_received(:<<)
                              .with("--hostname #{subject.hostname}")
        end

        it 'bind-mounds volume with logs in an appropriate directory' do
          allow(Gitlab::QA::Runtime::Env)
            .to receive(:host_artifacts_dir)
                  .and_return('/tmp/gitlab-qa/gitlab-qa-run-2018-07-11-10-00-00-abc123')

          subject.name = 'my-gitlab'

          subject.start

          expect(docker).to have_received(:volume)
            .with('/tmp/gitlab-qa/gitlab-qa-run-2018-07-11-10-00-00-abc123/my-gitlab/logs', '/var/log/gitlab', 'Z')
        end

        context 'with a network' do
          before do
            subject.network = 'testing-network'
          end

          it 'specifies the network' do
            subject.start

            expect(docker).to have_received(:<<)
                                .with('--net testing-network')
          end
        end

        context 'with volumes' do
          before do
            subject.volumes = { '/from' => '/to' }
          end

          it 'adds --volume switches to the command' do
            subject.start

            expect(docker).to have_received(:volume)
                                .with('/from', '/to', 'Z')
          end
        end

        context 'with environment' do
          before do
            subject.environment = { 'TEST' => 'some value' }
          end

          it 'adds environment variables to the command' do
            subject.start

            expect(docker).to have_received(:env)
                                .with('TEST', 'some value')
          end
        end

        context 'with network_alias' do
          before do
            subject.add_network_alias('lolcathost')
          end

          it 'adds --network-alias switches to the command' do
            subject.start

            expect(docker).to have_received(:<<).with('--network-alias lolcathost')
          end
        end

        describe 'with tls cert volumes' do
          let(:cert_path) { File.expand_path('../../../../tls_certificates', __dir__) }

          let(:alpine_helper) do
            instance_double(
              Gitlab::QA::Component::Alpine,
              :volumes= => nil,
              :start_instance => nil,
              :name => 'alpine',
              :teardown! => nil
            )
          end

          let(:cert_volumes) do
            {
              'authority' => '/etc/gitlab/trusted-certs',
              'gitlab-ssl' => '/etc/gitlab/ssl'
            }
          end

          before do
            allow(Gitlab::QA::Component::Alpine).to receive(:perform).and_yield(alpine_helper)
          end

          it 'creates volumes with tls certs', :aggregate_failures do
            subject.prepare

            expect(alpine_helper).to have_received(:volumes=).with(cert_volumes)
            expect(alpine_helper).to have_received(:start_instance)
            expect(alpine_helper).to have_received(:teardown!)

            expect(docker).to have_received(:execute)
              .with("cp #{cert_path}/authority/. alpine:#{cert_volumes['authority']}").once
            expect(docker).to have_received(:execute)
              .with("cp #{cert_path}/gitlab/. alpine:#{cert_volumes['gitlab-ssl']}").once
          end
        end
      end

      describe '#seed_db' do
        let(:expect_empty) { false }
        let(:file_patterns) {}
        let(:docker_engine) { spy('docker engine') }
        let(:exec_commands) { [] }
        let(:seed_db_dir) do
          dir = File.expand_path("/tmp/gitlab-qa-gitlab-rb-spec-#{SecureRandom.hex(10)}", __dir__)
          FileUtils.mkdir_p(dir)
          FileUtils.touch("#{dir}/test_file1.rb")
          FileUtils.touch("#{dir}/test_file2.rb")
          FileUtils.touch("#{dir}/file3.rb")
          dir
        end

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
          stub_const('Gitlab::QA::Component::Gitlab::DATA_SEED_PATH', seed_db_dir)
          stub_const('Gitlab::QA::Component::Gitlab::DATA_PATH', '/d/e/f')

          subject.instance_variable_set(:@exec_commands, exec_commands)
          subject.instance_variable_set(:@seed_admin_token, false)
          subject.instance_variable_set(:@seed_db, seed_db)
          allow(Runtime::Scenario).to receive(:seed_db).and_return(file_patterns)

          subject.process_exec_commands
        end

        context 'when seed_db is true' do
          let(:seed_db) { true }

          shared_examples 'exec docker commands when instance is ready' do
            it 'copies the data seed path to data path' do
              expect(docker_engine).to have_received(:copy).with(subject.name, seed_db_dir, '/d/e/f')
            end

            it 'adds the seed test data command to the exec_commands' do
              expected_commands = subject.send(:seed_test_data_command)
              expect(expected_commands).not_to be_empty unless expect_empty

              expect(exec_commands).to include(expected_commands)
            end
          end

          context 'with duplicated search pattern' do
            let(:file_patterns) { %w[test*.rb test_file1.rb] }

            it_behaves_like 'exec docker commands when instance is ready'
          end

          context 'with all seed scripts' do
            let(:file_patterns) { ['*'] }

            it_behaves_like 'exec docker commands when instance is ready'
          end

          context 'without matches' do
            let(:file_patterns) { ['test_file1'] }
            let(:expect_empty) { true }

            it_behaves_like 'exec docker commands when instance is ready'
          end
        end

        context 'when `--seed-db` is not set' do
          let(:seed_db) { false }
          let(:file_patterns) { ['file3.rb'] }

          it 'does not execute the seed_test_data script' do
            expect(subject).not_to receive(:seed_test_data_command)
          end
        end
      end

      describe '#seed_admin_token' do
        let(:docker_engine) { spy('docker engine') }
        let(:exec_commands) { [] }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
          stub_const('Gitlab::QA::Component::Gitlab::DATA_SEED_PATH', '/a/b/c')
          stub_const('Gitlab::QA::Component::Gitlab::DATA_PATH', '/d/e/f')
          subject.instance_variable_set(:@seed_admin_token, seed_admin_token)
          subject.instance_variable_set(:@exec_commands, exec_commands)
          subject.process_exec_commands
        end

        context 'when seed_admin_token is true' do
          let(:seed_admin_token) { true }

          it 'copies the data seed path to data pat' do
            expect(docker_engine).to have_received(:copy).with(subject.name, '/a/b/c', '/d/e/f')
          end

          it 'adds the seed admin token command to the exec_commands' do
            expect(exec_commands).to include(subject.send(:seed_admin_token_command))
          end
        end

        context 'when seed_admin_token is false' do
          let(:seed_admin_token) { false }

          it 'does not copy the data seed path to data path' do
            expect(docker_engine).not_to have_received(:copy)
          end

          it 'does not add the seed admin token command to the exec_commands' do
            expect(exec_commands).not_to include(subject.send(:seed_admin_token_command))
          end
        end
      end

      describe '#teardown' do
        let(:docker_engine) { spy('docker engine') }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
        end

        context 'when `--no-teardown` is set' do
          it 'leaves containers running' do
            allow(subject).to receive(:teardown?).and_return(false)

            subject.teardown

            expect(docker_engine).not_to have_received(:stop)
            expect(docker_engine).not_to have_received(:remove)
            expect(docker_engine).to have_received(:ps)
          end
        end

        context 'when `--no-teardown` is not set' do
          it 'stops and removes containers' do
            allow(subject).to receive(:teardown?).and_return(true)

            subject.teardown

            expect(docker_engine).to have_received(:remove)
            expect(docker_engine).not_to have_received(:ps)
          end
        end
      end

      describe '#server_hooks' do
        let(:docker_engine) { spy('docker engine') }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
          subject.instance_variable_set(:@skip_server_hooks, skip_server_hooks)
        end

        context 'when skip_server_hooks is false' do
          let(:skip_server_hooks) { false }

          it 'adds git server hooks to exec_commands' do
            expect(Support::ConfigScripts).to receive(:add_git_server_hooks).with(docker_engine, subject.name)
            subject.process_exec_commands
          end
        end

        context 'when skip_server_hooks is true' do
          let(:skip_server_hooks) { true }

          it 'does not add git server hooks to exec_commands' do
            expect(Support::ConfigScripts).not_to receive(:add_git_server_hooks)
            subject.process_exec_commands
          end
        end
      end

      describe '#reconfigure' do
        let(:docker) { spy('docker') }

        context 'with default omnibus configuration' do
          before do
            stub_const('Gitlab::QA::Support::ShellCommand', docker)
            Runtime::Scenario.define(:omnibus_configuration, Runtime::OmnibusConfiguration.new)
          end

          it 'configures omnibus by writing gitlab.rb' do
            subject.reconfigure

            expect(docker).to have_received(:new)
              .with(eq("docker exec #{subject.name} bash -c \"echo \\\"#{Runtime::Scenario.omnibus_configuration.to_s.gsub(
                '"', '\\"')}\\\" > /etc/gitlab/gitlab.rb;\""), anything)
          end
        end

        context 'with secrets to mask' do
          before do
            stub_const('Gitlab::QA::Docker::Engine', docker)
            subject.secrets = ['secret']
          end

          it 'passes secrets to docker engine' do
            subject.reconfigure

            expect(docker).to have_received(:write_files).with(subject.name, { mask_secrets: ['secret'] })
          end
        end

        context 'without secrets to mask' do
          before do
            stub_const('Gitlab::QA::Docker::Engine', docker)
          end

          it 'does not pass any secrets to docker engine' do
            subject.reconfigure

            expect(docker).to have_received(:write_files).with(subject.name, { mask_secrets: [] })
          end
        end
      end

      describe '#create_key_file' do
        let(:docker) { spy('docker') }

        around do |example|
          ClimateControl.modify(MY_KEY: 'key') { example.run }
        end

        it 'copies a key file' do
          file_path = subject.create_key_file('MY_KEY')

          stub_const('Gitlab::QA::Docker::Command', docker)
          allow(subject).to receive(:ensure_configured!)

          subject.start

          expect(docker).to have_received(:volume)
                                .with(file_path, file_path, 'Z')
        end
      end

      describe '#package_version' do
        it 'returns locked version' do
          allow(subject).to receive(:read_package_manifest)
            .and_return('{"software":{"package-scripts":{"locked_version":"15.1.2"}}}')

          expect(subject.package_version).to eq('15.1.2')
        end
      end

      describe '#exist' do
        let(:docker) { spy('docker') }

        it 'calls and returns docker exist true' do
          stub_const('Gitlab::QA::Docker::Engine', docker)

          expect(docker).to receive(:manifest_exists?).with('foo/bar:xyz').and_return(true)

          expect(subject.exist?('foo/bar', 'xyz')).to be(true)
        end

        it 'calls and returns docker exist false' do
          stub_const('Gitlab::QA::Docker::Engine', docker)

          expect(docker).to receive(:manifest_exists?).with('bar/foo:xyz').and_return(false)

          expect(subject.exist?('bar/foo', 'xyz')).to be(false)
        end
      end

      describe '#process_exec_commands' do
        let(:docker) { spy('docker') }
        let(:secret) { 'user-agent-secret' }
        let(:command) { "command with secret: #{secret}" }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker)
          allow(Runtime::Scenario).to receive(:omnibus_exec_commands).and_return([command])
          allow(Runtime::Scenario).to receive(:seed_admin_token).and_return(false)
        end

        context 'with secrets to mask' do
          it 'passes secrets to docker engine' do
            subject.secrets = [secret]
            subject.process_exec_commands

            expect(docker).to have_received(:exec).with(subject.name, command, { mask_secrets: [secret] })
          end
        end

        context 'without secrets to mask' do
          it 'does not pass any secrets to docker engine' do
            subject.process_exec_commands

            expect(docker).to have_received(:exec).with(subject.name, command, { mask_secrets: [] })
          end
        end
      end

      describe '#set_qa_user_agent' do
        around do |example|
          ClimateControl.modify(GITLAB_QA_USER_AGENT: 'user-agent-secret') { example.run }
        end

        it 'sets GITLAB_QA_USER_AGENT as a rails env var' do
          subject.set_qa_user_agent

          expect(subject.omnibus_gitlab_rails_env).to include({ 'GITLAB_QA_USER_AGENT' => 'user-agent-secret' })
        end

        it 'treats the value of GITLAB_QA_USER_AGENT as a secret' do
          subject.set_qa_user_agent

          expect(subject.secrets).to include('user-agent-secret')
        end
      end

      private

      def create_release(release)
        Release.new(release)
      end
    end
  end
end
