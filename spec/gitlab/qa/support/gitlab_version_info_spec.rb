# frozen_string_literal: true

require "logger"

describe Gitlab::QA::Support::GitlabVersionInfo do
  subject(:previous_version) { described_class.new(current_version, edition).previous_version(semver_component) }

  let(:current_version) { "15.3.0-pre" }
  let(:edition) { "ee" }
  let(:tags) do
    <<~JSON
      [
        {"layer": "", "name": "latest"},
        {"layer": "", "name": "14.8.0-ee.0"},
        {"layer": "", "name": "14.9.5-ee.0"},
        {"layer": "", "name": "15.1.0-ee.0"},
        {"layer": "", "name": "15.2.1-ee.0"}
      ]
    JSON
  end

  before do
    allow(Gitlab::QA::Runtime::Logger).to receive(:logger) { Logger.new(StringIO.new) }

    stub_request(:get, "https://registry.hub.docker.com/v2/namespaces/gitlab/repositories/gitlab-ee/tags?page_size=1000")
      .with(body: "{}")
      .to_return(status: code, body: %({ "results": #{tags} }))
  end

  [
    { semver: "major", latest: Gem::Version.new("14.9.5"), latest_fallback: Gem::Version.new("14.0.0") },
    { semver: "minor", latest: Gem::Version.new("15.2.1"), latest_fallback: Gem::Version.new("15.2.0") },
    { semver: "patch", latest: Gem::Version.new("15.2.1"), latest_fallback: Gem::Version.new("15.2.0") }
  ].each do |params|
    context "with previous major version" do
      let(:semver_component) { params[:semver] }

      context "with successfull response" do
        let(:code) { 200 }

        it "returns correct previous latest major version" do
          expect(previous_version).to eq(params[:latest])
        end
      end

      context "with unsuccessfull response" do
        let(:code) { 500 }

        it "returns correct previous fallback major version" do
          expect(previous_version).to eq(params[:latest_fallback])
        end
      end
    end
  end
end
