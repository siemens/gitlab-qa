# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '11.3.0'
  end
end
